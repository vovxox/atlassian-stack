#!/bin/bash
echo "******CREATING STASH DATABASE*****"
psql --username postgres <<- EOSQL
  CREATE DATABASE stash;
  CREATE USER stash WITH PASSWORD 'test123';
  ALTER USER stash WITH SUPERUSER;
EOSQL
echo ""

{ echo; echo "host stash stash 0.0.0.0/0 trust"; } >> "$PGDATA"/pg_hba.conf

if [ -r '/tmp/dumps/stash.dump' ]; then
    echo "**IMPORTING STASH DATABASE BACKUP**"
    gosu postgres postgres &
    SERVER=$!; sleep 2
    gosu postgres psql stash < /tmp/dumps/stash.dump
    kill $SERVER; wait $SERVER
    echo "**STASH DATABASE BACKUP IMPORTED***"
fi

echo "******STASH DATABASE CREATED******"
