#!/bin/bash
echo "******CREATING CONFLUENCE DATABASE******"
psql --username postgres <<- EOSQL
  CREATE DATABASE confluence;
  CREATE USER confluence WITH PASSWORD 'test123';
  ALTER USER confluence WITH SUPERUSER;
EOSQL
echo ""
echo ""

{ echo; echo "host confluence confluence 0.0.0.0/0 trust"; } >> \
  "$PGDATA"/pg_hba.conf

if [ -r '/tmp/dumps/confluence.dump' ]; then
    echo "**IMPORTING CONFLUENCE DATABASE BACKUP**"
    gosu postgres postgres &
    SERVER=$!; sleep 2
    gosu postgres psql confluence < /tmp/dumps/confluence.dump
    kill $SERVER; wait $SERVER
    echo "**CONFLUENCE DATABASE BACKUP IMPORTED***"
fi

echo "******CONFLUENCE DATABASE CREATED******"
