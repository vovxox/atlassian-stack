#!/bin/bash
echo "******CREATING JIRA DATABASE******"
psql --username postgres <<- EOSQL
  CREATE DATABASE jira;
  CREATE USER jira WITH PASSWORD 'test123';
  ALTER USER jira WITH SUPERUSER;
EOSQL
echo ""

{ echo; echo "host jira jira 0.0.0.0/0 trust"; } >> "$PGDATA"/pg_hba.conf

if [ -r '/tmp/dumps/jira.dump' ]; then
    echo "**IMPORTING JIRA DATABASE BACKUP**"
    gosu postgres postgres &
    SERVER=$!; sleep 2
    gosu postgres psql jira < /tmp/dumps/jira.dump
    kill $SERVER; wait $SERVER
    echo "**JIRA DATABASE BACKUP IMPORTED***"
fi

echo "******JIRA DATABASE CREATED******"
