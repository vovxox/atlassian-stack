#!/bin/bash
echo "******CREATING BAMBOO DATABASE******"
psql --username postgres <<- EOSQL
  CREATE DATABASE bamboo;
  CREATE USER bamboo WITH PASSWORD 'test123';
  ALTER USER bamboo WITH SUPERUSER;
EOSQL
echo ""

{ echo; echo "host bamboo bamboo 0.0.0.0/0 trust"; } >> "$PGDATA"/pg_hba.conf

if [ -r '/tmp/dumps/bamboo.dump' ]; then
    echo "**IMPORTING BAMBOO DATABASE BACKUP**"
    gosu postgres postgres &
    SERVER=$!; sleep 2
    gosu postgres psql bamboo < /tmp/dumps/bamboo.dump
    kill $SERVER; wait $SERVER
    echo "**BAMBOO DATABASE BACKUP IMPORTED***"
fi

echo "******BAMBOO DATABASE CREATED******"
